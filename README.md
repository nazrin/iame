# Ika's Adequate Math Evaler
Evals mathematical expressions without actually evaling code

# API

Functions
* `Evaler iame:evaler()` - New instance
* `number Evaler:eval(strOrTable)` - Evals and returns the string or table of tokens
* `table Evaler:lex(str)` - Turns a string into a table of tokens like `{ 'sin', '(', 5, '*', 5, ')' }`
* `int Evaler:getPrecedence(op)` - Returns precedence of op

Tables
* `Evaler.constants`  - Constants like `pi` or `e`
* `Evaler.functions`  - Functions with any number of arguments like `sin`, `pow`, `random`
* `Evaler.operators`  - Binary (2 args) functions like `+` or `*`
* `Evaler.uoperators` - Unary (1 arg) functions like `-`
* `Evaler.precedence` - Numbers giving relative priority, `+-` = 10, `*/%` = 20, `^` = 30

In case you want to extend them a bit:
* `Evaler.lexerRegex` - `require("regex").new("blablabla")`
* `Evaler.tonumber` - Default `tonumber`

By default has all the same operators as Lua and all the constants/functions from `math` except `math.randomseed` plus a few constants and a `round` function

No binary ops but they're trivial to add. See second example

# Examples

## General
```lua
local evaler = require("iame").evaler()

print(evaler:eval("2 + 2"))         --> 4
print(evaler:eval("2 ^ (2 * 2)"))   --> 16
print(evaler:eval("pow(2, 2 * 2)")) --> 16
print(evaler:eval("deg(rad(5))"))   --> 5
print(evaler:eval("round(pi * 2)")) --> 6
print(evaler:eval("random()"))      --> 0.80960292833219
print(evaler:eval("random(1, 10)")) --> 7
if type(jit) == "table" then -- luajit
	print(evaler:eval("0b010 + 0b101")) --> 7 (0b111)
end

function evaler.functions.hello(n)
	return (n or 0) + 5
end
print(evaler:eval("hello()"))  --> 5
print(evaler:eval("hello(5)")) --> 10

function evaler.operators.yes(a, b)
	return a + b * a + b
end
print(evaler:eval("5 yes 6")) --> 41

evaler.uoperators["!"] = function(n)
	return n == 0 and 1 or n * evaler.uoperators["!"](n - 1)
end
print(evaler:eval("!5")) --> 120
```

## Binary
```lua
local evaler = require("iame").evaler()

local function addBinaryOpsToEvaler(evaler)
	local bit = require("bit") -- https://bitop.luajit.org/index.html luarocks install luabitop
	evaler.uoperators["~"] = bit.bnot
	local ops = { ["&"] = "band", ["|"] = "bor", ["~"] = "bxor", ["<<"] = "lshift", [">>"] = "rshift" }
	for op,fun in pairs(ops) do
		evaler.operators[op] = bit[fun]
		evaler.precedence[op] = 40
	end
end
addBinaryOpsToEvaler(evaler)

print(evaler:eval("0b0101 & 0b0110")) --> 0b0100
print(evaler:eval("0b0101 | 0b0110")) --> 0b0111
print(evaler:eval("0b0101 ~ 0b0110")) --> 0b0011
print(evaler:eval("0b010 << 1"))      --> 0b100
print(evaler:eval("0b010 >> 1"))      --> 0b001
print(evaler:eval("~0"))              --> -1
```

# Limitations

* No `314.16e-2` formatted numbers
* Numbers limited by `tonumber` (or your own provided function) so no `0b101` numbers in PUC Lua
* Weak order of operations for unary ops: `!5 + 5` = 3628800, not 125
	* It's still possible to use functions instead such as `fact(5) + 5` or even `!(5) + 5`
* Don't combine unary ops with binary: `5 - -(5)`

