---
-- @module iame
-- @license GPL-v3
-- @author nazrin
local iame = {}

local Evaler = {}
Evaler.__index = Evaler
local lexerRegex = require("regex").new("(([*><])\\2)|([a-zA-Z]+)|((?<![0-9])-?(0[xob])?[0-9a-fA-F.]+)|([^\\s])", "g")

--- Turns a string into a table of tokens
-- @string str
function Evaler:lex(str)
	local tokens = self.lexerRegex:matches(str)
	for i,x in ipairs(tokens) do
		local n = self.tonumber(x)
		if n then
			tokens[i] = n
		elseif self.operators[x] or self.uoperators[x] or x:match("[(),]") then
			-- noop
		elseif tokens[i+1] == "(" then
			-- noop
		elseif self.constants[x] then
			tokens[i] = self.constants[x]
		else
			error(("Unknown token: %q"):format(x))
		end
	end
	return tokens
end
--- Evals a string or table of tokens and returns the result
-- @param strOrTableOfTokens
-- @return number
function Evaler:eval(tokens)
	if type(tokens) == "string" then
		tokens = self:lex(tokens)
	end
	local vstack, ostack = {}, {}
	local function doOp(op)
		local v2 = table.remove(vstack)
		local v1 = table.remove(vstack)
		if not op then
			op = table.remove(ostack)
		end
		assert(op, "No operator")
		local res
		if v1 then -- binary
			local opf = self.operators[op]
			assert(opf, ("No operator: %q"):format(op))
			res = opf(v1, v2)
		else -- unary
			local opf = self.uoperators[op]
			assert(opf, ("No unary operator: %q"):format(op))
			res = opf(v2)
		end
		table.insert(vstack, res)
	end
	local i = 1
	while tokens[i] do
		local token = tokens[i]
		if token == "(" then
			table.insert(ostack, token)
		elseif type(token) == "number" then
			table.insert(vstack, token)
		elseif token == ")" then
			while #ostack > 0 and ostack[#ostack] ~= "(" do
				doOp()
			end
			table.remove(ostack)
		elseif self.operators[token] or self.uoperators[token] then
			while (#ostack > 0) and (self:getPrecedence(ostack[#ostack]) >= self:getPrecedence(token)) do
				doOp()
			end
			table.insert(ostack, token)
		else
			local f = self.functions[token]
			local args = {{}}
			local brackets = 1
			i = i + 2
			while tokens[i] do
				if tokens[i] == ")" then
					brackets = brackets - 1
				elseif tokens[i] == "(" then
					brackets = brackets + 1
				end
				if brackets == 0 then
					break
				end
				if tokens[i] == "," and brackets == 1 then
					table.insert(args, {})
				else
					table.insert(args[#args], tokens[i])
				end
				i = i + 1
			end
			local ret
			if #args[1] > 0 then
				for o=1,#args do
					args[o] = self:eval(args[o])
				end
				ret = f((table.unpack or unpack)(args))
			else
				ret = f()
			end
			table.insert(vstack, ret)
		end
		i = i + 1
	end
	while #ostack > 0 do
		local op = table.remove(ostack)
		if op ~= "(" and op ~= ")" then
			doOp(op)
		end
	end
	if #vstack > 1 then
		for i,x in ipairs(tokens) do tokens[i] = tostring(x) end
		error(("Excess tokens: %s"):format(table.concat(tokens, " ")))
	end
	return vstack[1]
end
--- Returns the precedence of an operation
-- @string op
-- @return int
function Evaler:getPrecedence(op)
	return self.precedence[op] or 0
end

--- Creates a new evaler
-- @return Evaler
function iame.evaler()
	local evaler = { lexerRegex = lexerRegex, tonumber = tonumber }
	evaler.constants = {
		pi = math.pi, e = 2.718281828459045, ln2 = 0.6931471805599453,
		ln10 = 2.302585092994046, log2e = 1.4426950408889634, log10e = 0.4342944819032518
	}
	evaler.operators = {
		["+"] = function(a, b) return a + b end,
		["-"] = function(a, b) return a - b end,
		["*"] = function(a, b) return a * b end,
		["/"] = function(a, b) return a / b end,
		["%"] = function(a, b) return a % b end,
		["^"] = function(a, b) return a ^ b end,
	}
	evaler.uoperators = {
		["-"] = function(n) return -n end,
	}
	evaler.precedence = {
		["+"] = 10, ["-"] = 10,
		["*"] = 20, ["/"] = 20, ["%"] = 20,
		["^"] = 30,
	}
	evaler.functions = {}
	function evaler.functions.round(n)
		return math.floor(n+0.5)
	end
	for _,f in ipairs({
		"abs", "acos", "asin", "atan", "atan2", "ceil", "cos", "cosh", "deg", "exp", "floor", "fmod", "frexp",
		"ldexp", "log", "log10", "max", "min", "modf", "pow", "rad", "random", "sin", "sinh", "sqrt", "tan", "tanh"
	}) do
		evaler.functions[f] = math[f]
	end

	return setmetatable(evaler, Evaler)
end

return iame

