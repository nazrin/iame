local iame = require("iame")
local evaler = iame.evaler()

local function test(str, t)
	local ok, err = pcall(function()
		local res = evaler:eval(str)
		local f, err = loadstring("return " ..str)
		local fres = f()
		assert((fres == res) or (fres ~= fres), str, fres) -- I HATE THE IEEE
		local nf = math.floor(res) == res and "d" or "f"
		print((("%%s = %%%s"):format(nf)):format(str, res))
		if not t then
			local f = "sin"
			test(("\t%s(%s)"):format(f, str), 1)
		elseif t == 1 then
			test(("\t\tpow(%s * 10, 9) + 100"):format(str), true)
		end
	end)
	if not ok then error(str .. "\n\t" .. err) end
end

for i,x in pairs(math) do
	_G[i] = x
end
function _G.round(n)
	return floor(n+0.5)
end

test("1")
test("-1")
test("-(1)")
test("-(1 + 5)")
test("2 + 2")
test("5*5")
test("sqrt(3+3+3)")
test("round(pow(pi * 2, 2)) + 1")
test("(1-0.25) * 1 + 0.25 * 5")
test(" 1 + (5*2) - 2 * 7 - 1 + 5 + 34.5555 * 1")
test(" (1 + (5*2) - 2 * 7 - 1 + 5 + 34.5555 * 1)")
test(" (((1 + (5*2))) - 2 * 7 - 1 + 5 + 34.5555 * 1)")
test(" (((1 + (5*2)) - 2) / 7 - 1 + 5 + 34.5555 * 1)")
test("pow(1,5) * sin(pi)")
test("pow(1,5) * sin(pi) + 59 % 6 ^ 5")
test("pow(1,5) * sin(pi) + (59 % 6 ^ 5)")
test("floor((sqrt(999.9 * 4 / 25 + 1) - 1) / 2)")
test("5 % 2")
test("rad(deg(5))")
test(".41 * 1.2")
test("2. * ((((0xff))) - 0.31416E1)")
if type(jit) == "table" then -- luajit
	test("0b101 + 0b010")
end

assert(evaler:eval("pi * 2") == math.pi * 2)
assert(evaler:eval("1 + 1") == 2)
assert(evaler:eval("rad(deg(1))") == 1)

local function shouldFail(str)
	local ok, err = pcall(evaler.eval, evaler, str)
	assert(not ok, str .. "\n" .. err)
end

shouldFail("1 1")
shouldFail("f23f023f02")
shouldFail("11 ++ 1")
shouldFail("11 +  0 0 + 1")
shouldFail("sin()")
shouldFail("pow(0,)")
shouldFail("pow(,)")
shouldFail("+")
shouldFail("")
shouldFail("1999n")
shouldFail("1 == 1")
shouldFail("* *")
shouldFail("* 6 *")

assert(evaler:eval("random()") ~= evaler:eval("random()"))

